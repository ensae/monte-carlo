# Projet de "Simulation et Monte Carlo" - réseaux sociaux

*Par Leena Boyina, Thomas Garnier et Guillaume Lévy.*

## Un peu de contexte

Notre point de départ est l'article [Latent Space Approaches to Social Network Analysis](https://www.stat.cmu.edu/~brian/905-2009/all-papers/hoff-raftery-handcock-2002-jasa.pdf), publié par Peter D. Hoff, Adrian E. Raftery et Mark S. Handcock, publié en 2002 dans le prestigieux *Journal of the American Statistical Association*.

Dans cet article, les auteurs introduisent la notion d'*espace de variables latentes*, que l'on abrègera parfois en *espace latent* par commodité. Cette notion abstraite vise à opérer une réduction de dimension sur de grands graphes dits sociaux, i.e. des graphes construits à partir d'exemples réels de relations entre personnes ou entités. De tels graphes présentent par nature des corrélations positives entre les arètes, formant ainsi des grappes (*clusters*) de sommets à forte connectivité interne (proches de former des sous-cliques).

Le problème se pose ainsi : peut-on trouver un espace euclidien *de faible dimension* dans lequel plonger l'ensemble des sommets, de telle sorte que la "proximité" dans le graphe d'origine puissent se lire directement dans cet espace ? 

L'article apporte une réponse positive à cette question, basé sur l'algorithme de Metropolis-Hastings, en illustrant les résultats obtenus sur quelques exemples classiques de la littérature. Les mariages entre grandes familles pendant la Renaissance italienne, les relations d'amitié entre moines d'un couvent et les relations d'amitié entre jeunes enfants dans une école sont traités et commentés. 

## Objectif et résultats

Notre objectif, tout modestement, est de reproduire les résultats de cet article sur au moins un exemple. Nous produisons une implémentation en Python de l'algorithme principal et simulons effectivement la construction d'une configuration de points dans l'espace latent.


## Licence

Ce projet est publié sous la licence GPLv3.


